\mainpage

Especificaciones del software
=====================

PROBADOR.


USO DE PINES Y PUERTOS EN PROTOTIPO
=====================================

Pin |  Nombre en micro | Nombre en placa| Conector / Pin | Nombre en software    |  Uso                            | 
----| -------------    | -------------- | -------------  | ---------             |    -------------                |
1	| PTC4  		   | Control2_P6    | Control2, 6    | BUZZER_PIN            | Buzzer                          |
2	| IRQ  		  	   | - 	            |    -           | --                    |    -                            |
3	| RESET  		   | RESET          |    -           | --                    |    -                            |
4	| PTF0/TPM1HC2     | Control1_P4    | Control1, 5    | PIN_CH2_PWM      |  Detección de PWM en canal 2            |
5	| PTF1/TPM1CH3     | Control1_P3    | Control1, 4    | PIN_CH1_PWM      |  Detección de PWM en canal 1            |
6	| PTF4/TPM2CH0 	   | Control1_P2    | Control1, 3    | PIN_ACOPLE_2      |  Detección de inducción en canal 2            |
7	| PTF5/TPM2CH1     | Control1_P1    | Control1, 2    | PIN_ACOPLE_1      |  Detección de inducción en canal 1            |
8	| PTE0  		   | Display_D0     | LCD, 7         |         | DATOS DISPLAY                      |
9	| PTE1  		   | Display_D1     | LCD, 8         |         | DATOS DISPLAY                      |
10	| PTE2/TPM1CH0     | Display_D2     | LCD, 9         |         | DATOS DISPLAY                      |
11	| PTE3/TPM1CH1     | Display_D3     | LCD, 10        |         | DATOS DISPLAY                      |
12	| PTE4  		   | Display_D4     | LCD, 11        |         | DATOS DISPLAY                      |
13	| PTE5  		   | Display_D5     | LCD, 12        |         | DATOS DISPLAY                      |
14	| PTE6  		   | Display_D6     | LCD, 13        |         | DATOS DISPLAY                      |
15	| PTE7  		   | Display_D7     | LCD, 14        |         | DATOS DISPLAY                      |
16	| VSS  		       | GND            | -              | --                    |                 |
17	| VDD  		       | VDD            | -              | --                    |                 |
18	| PTG0/KBIP0  	   | Kby_0          | Teclado, 1     | keyStart              |                 |
19	| PTG1/KBIP1  	   | Kby_1          | Teclado, 2     | --                    |                 |
20	| PTG2/KBIP2  	   | Kby_2          | Teclado, 3     | keyFreq               |                 |
21	| PTA0  		   | E              | LCD, 6         |                       | Control display                 |
22	| PTA1  		   | RS             | LCD, 4         |                       | Control display                 |
23	|PTB0/TPM3CH0/AD1P0| Aux_P3         | AUX, 5         |  --                   |  |
24	|PTB1/TPM3CH1/AD1P1| Aux_P2         | AUX, 4         | PIN_CARGADOR_ENABLE   | Habilita el cargador |
25	| PTB2/AD1P2       | Aux_P1         | AUX, 3         | PIN_CORTO1   | Hace cortocircuito en la salida 1 |
26	| PTB3/AD1P3       | Aux_P0         | AUX, 2         | PIN_CORTO2   | Hace cortocircuito en la salida 2 |
27	| PTD0/AD1P8  	   | Kby_7          | Teclado, 5     |               |  |
28	| PTD1/AD1P9  	   | Kby_8          | Teclado, 6     |     		 |  |
29	| VDDAD  		   | VDD            | -              |  --                   |  |                
30	| VSSAD  		   | GND            | -              |  --                   |  |                
31	| PTD2/KBIP5/AD1P10| Kby_5          | Teclado, 7     |      		 |  |
32	| PTD3/KBIP6/AD1P11| Kby_6          | Teclado, 8     |      		 |  |
33	| PTG3/KBIP3  	   | Kby_3          | Teclado, 4     |           |  |
34	| VREFH  		   | VDD            | -              |  --                   |  |
35	| VREFL  		   | GND            | -              | ---                   |  |
36	| BKGD  		   | BKGD           | -              | ---                   |  |
37	| PTG5/XTAL  	   | --             | -              |  --                   |  |
38	| PTG6/EXTAL  	   | --             | -              |  --                   |  |
39	| VSS  		       | GND            | -              |  --                   |  |
40	| PTC0  		   | Control2_P1    | Control 2, 5   |  --                   |  |
41	| PTC1  		   | Control2_P2    | Control 2, 4   |  --                   |  |
42	| PTC2 		  	   | Control2_P3    | Control 2, 3   |  --                   |  |
43	| PTC3 		       | Control2_P4    | Control 2, 2   |  --                   |  |
44	| PTC5 		       | Control2_P5    | Control 2, 1   |  --                   |  |


TIMERS
===========

Timer | Periodo 	| Uso  |
----- | ------- 	| ---  |
 1    | 1 ms		| Reloj|
 2    |  --  		|   |
 3    | No se usa   |      |
