/*
 * display.c
 *
 *  Created on: 08-oct-2015
 *      Author: czelayeta
 */

#include "derivative.h"
#include "LCD.h"
#include "display.h"

extern unsigned char temp[16];
extern unsigned char display_buffer[32];
unsigned char line2[50];

/** Actualiza el tiempo restante de tratamiento.
 * 
 * ¿Quién la llama?
 */
void actualizaHora(unsigned char min,unsigned char sec){	
	show_time(display_buffer,min,10); 
	show_time(display_buffer,sec,13);
	*(display_buffer+13)=':';
	lcd_cmd(0x02);
	show();	
}

/** Muestra el tiempo restante de tratamiento
 * \param destino Dirección a la que envío el dato.
 * \param n Por lo visto, es el string a escribir.
 * \param position Posición en la que voy a escribir el dato.
 * 
 * Se llama solamente por actualizaHora()
 */
void show_time(unsigned char* destino,unsigned char n,unsigned char position) 
{	
	unsigned char units;
	unsigned char lcd_pos;
	unsigned char aux;
	unsigned char numero;
	numero=n;
	units=numero;
	lcd_pos=1;
	while(units>=10) 
	{
	  units=units/10;
	  lcd_pos++;
	}
	aux=lcd_pos;   
	for (lcd_pos=2;lcd_pos!=0;lcd_pos--)
	{
	  units=numero/10;
	  *(destino+position+lcd_pos)=48+(numero-(units*10));  // tomo el resto de la division
	  numero=units;
	}
}

/** Escribe decimal al lcd.
 * 
 */
void write_dec_to_lcd(unsigned char* destino,unsigned int numero,unsigned char position) 
{
	unsigned int units;
	unsigned char lcd_pos;
	unsigned char aux;
	// Cuantos digitos tiene ? ... me fijo para saber el lugar que tengo que reservar en dsiplay 
	units=numero;
	lcd_pos=1;
	while(units>=10) 
	{
	  units=units/10;
	  lcd_pos++;
	}
	// Ahora lcd_pos contiene la cantidad de caracteres que ocupara en display ....
	aux=lcd_pos; 
	while(lcd_pos>0)
	{
	  units=numero/10;
	  if(aux==1){
		  lcd_pos--;
		  *(destino+position+lcd_pos-2)=48+(numero-(units*10));  // tomo el resto de la division
		  *(destino+position+lcd_pos-1)='0';  
	  }else
		  *(destino+position+lcd_pos-2)=48+(numero-(units*10));  // tomo el resto de la division 
	  numero=units;
	  lcd_pos--;
	}
}
/** Descripcion: Muestra un string en el display
*	  Puntero a la cadena alojada en FLASH que se quiere mostrar
 */
void showMessage(const unsigned char* msg){	
	lcd_clear();										// Limpia pantalla
	lcd_put_string((unsigned char*)msg,getSize(msg));					// Muestra cadena en pantalla
}

/** Calcula el tamaño de una cadena de caracteres terminada en NULL
 * */
unsigned char getSize(char* buffer){
	unsigned char i;
	i=0;
	while(*(buffer+i)!=0x00){
		i++;
	}		
	return i;	
}

/** Escribe un numero en el buffer de pantalla
 * 
 */
void writeNumber(unsigned char num,unsigned char pos){	
	write_dec(display_buffer,num,pos);
}

/** Escribe un numero en el buffer de pantalla.
 * 
 * ¿Qué diferencia tiene con la otra, writeNumber() ?
 */
void writeNumber2(char* buffer,unsigned char num,unsigned char pos){	
	write_dec(buffer,num,pos);
}

/** Muestra numero en pantalla en la posición indicada.
 * */
void showNumber(unsigned char num,unsigned char p){	
write_dec(display_buffer,num,p);	
show();
}

/** Desliza la linea superior hacia la izq y la linea inferior hacia la derecha.
 * Parametros: mode:0 mode:1 SCROLL LEFT mode:3 SCROLL SOLO LINEA 2
 * */
void scroll_message(unsigned char mode){ 
	unsigned char i;
	//unsigned char j;
	unsigned int z;
	//unsigned char size;
	unsigned char line1[32];
	unsigned char line2[32];	
	if(mode!=3){
		memset(line1,0x20,sizeof(line1));
		copy2Buffer(display_buffer,line1,16);
	}
	memset(line2,0x20,sizeof(line2));
	if(!mode)
		copy2Buffer(display_buffer+16,line2+16,16);	
	else if(mode==1 || mode==3)
		copy2Buffer(display_buffer+16,line2,16);	
	lcd_clear();
	for (i=0;i<16;i++){	
		if(mode!=3){
			lcd_cmd(0x02);		
			lcd_put_string(line1+i,16);
		}		
		lcd_cmd(0xC0);				//Escribe en la linea inferior
		if(!mode)
			lcd_put_string(line2+16-i,16);
		else if (mode==1 || mode==3)
			lcd_put_string(line2+i,16);
		for (z=0;z<30000;z++){
			asm nop;
		}
	}
}

/** Pone en pantalla el contenido del buffer de pantalla.
 * No puede ser, pone otra cosa en pantalla, porque si no sería igual a show()
*/
void scroll(unsigned char index){
		lcd_cmd(0xC0);
		lcd_put_string(line2+index,16);	
}

/**Pone en pantalla el contenido del buffer de pantalla
 * */
void show (void){
	lcd_cmd(0x02);
	lcd_put_string(display_buffer,getSize(display_buffer));	
}

/** Limpia buffer de pantalla
 */
void clearBuffer(void){
	memset(display_buffer,0x20,sizeof(display_buffer));
}

/** Copia la cantidad de bytes especificada desde origen a destino
*/
void copy2Buffer(char* origen,char* destino,unsigned char size){
	unsigned char i;	
	for(i=0;i<size;i++)
		*(destino+i)=*(origen+i);		
}

/** ¿Escribe un decimal al display?
 * 
 */
void write_dec(unsigned char* destino,unsigned char n,unsigned char pos) {
	unsigned char units;
	unsigned char lcd_pos;
	unsigned char aux;
	unsigned char numero;
	numero=n;
	units=numero;
	lcd_pos=1;
	while(units>=10) 
	{
	  units=units/10;
	  lcd_pos++;
	}
	aux=lcd_pos;
	while(lcd_pos>0)
	{
	  units=numero/10;
	  *(destino+pos+lcd_pos)=48+(numero-(units*10));  // tomo el resto de la division
	  numero=units;
	  lcd_pos--;
	}
	if (aux==1 || aux==2)			 
		*(destino+aux+1+pos)=0x20;
}

