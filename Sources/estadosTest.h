/*
 * estadosTest.h
 *
 *  Created on: 20-oct-2016
 *      Author: czelayeta
 */

#ifndef ESTADOSTEST_H_
#define ESTADOSTEST_H_

typedef enum{
	wait,
	deteccionEquipo,
	deteccionInicioTest,
	deteccionEstimMax,
	falla,
	testCorto1,
	testCorto2,
	testAliment,
	waitChargeBatt,
	waitOff,
	verEstimMag,
	testOk,
}estado_t;

static void tpm2ch0FE(void);
static void tpm2ch1FE(void);
static void tpm2ch0RE(void);
static void tpm2ch1RE(void);


void testearCorto1(void);
void testearCorto2(void);
void detectarInicioTest(void);
void detectarEquipo(void);
void medirEstimulacion(void);
void conectarFuente(void);
void verificarCarga(void);
void esperarApagado(void);
void verCampo(void);

#endif /* ESTADOSTEST_H_ */
