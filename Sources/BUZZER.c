/**
 * \file BUZZER.c
 *
 *  \author Pablo Dardano
 *  
 */

#include "BUZZER.h"

/** Estructura con todas las propiedades del buzzer
 * 
 */
static bz buz;


/** Entrega el estado actual del buzzer
 * 
 * Simplemente encapsula la consulta a buz.state, para no acceder directamente a la variable.
 * */
unsigned char getBuzzerState(void){
	return buz.state;	
}


/** Seteo los parámetros del buzzer
 * 
 * \param beeps Cantidad de beeps que quiera hacer
 * \param ton Duración de cada beep
 * \param toff Duración de la pausa entre beeps
 * \param tend Finalización de la secuencia de beeps
 * */
void setupBuzzer(unsigned char beeps, unsigned int ton, unsigned int toff, unsigned int tend){
	buz.timeOn=ton;
	buz.timeOff=toff;
	buz.cantidad=beeps;	
	buz.ctOn=ton;
	buz.ctOff=toff;
	buz.tend=tend;
	buz.state=0;
	BUZZER=0;
	
}

/** Maquina de estados buzzer
 *
 * Corre en un timer, se llama cada 1 ms.
 *  * \dot
	digraph {
		label="Máquina de estados del buzzer"
		
		state[shape="diamond"];
		ctOn[shape="diamond", label="¿cont On=0?"];
		ctOff[shape="diamond", label="¿Cont off=0?"];
		output0[label="Apago buzzer"];
		output0[label="Prendo buzzer"];
		cantidad[shape="diamond", label="¿cantidad=0?"];
		
		state -> ctOn[label="0"];
		ctOn -> output0[label="sí"];
		output0->reinicioctOff;
		reinicioctOff -> estado1;
		ctOn -> decrementoctOn[label="No"];
		state -> ctOff[label="1"];
		ctOff->cantidad[label="sí"];
		cantidad -> apagoBuzzer [label="Sí"];
		ctOff-> decrementoctOff[label="No"];
	  }
 *  \enddot
*/
void doBeep(void){	
	if (buz.state==0){
		if(buz.ctOn == 0){ //si ctOn es igual a 0.
			BUZZER=0; //pongo en 0 el pin del buzzer
			buz.ctOff=buz.timeOff; //seteo el contador off en el timeOff
			buz.state=1; //cambio el estado a 1	
		}
		buz.ctOn--;
	}
	else if (buz.state==1){		
		if(buz.ctOff == 0){				
			if(buz.cantidad==0){
				BUZZER=0; //apago el buzzer
				buz.state=2;		//¿Qué mierda es este tercer estado?
			}
			else{									
				if(buz.tend>0 && buz.cantidad==1)
					buz.ctOn=buz.tend;
				else
					buz.ctOn=buz.timeOn;
				if(buz.cantidad>0)
					BUZZER=1;
				buz.state=0;					
			}
			buz.cantidad--;
		}
		buz.ctOff--;
	}	
}
	
