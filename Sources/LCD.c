/** Funciones de bajo nivel para el display. 
 * \file LCD.c
 * \author Pablo Dardano
 * 
 * Las funciones de alto nivel están en display.h
 * El display que maneja este archivo es de 16x2. Se maneja en modo 8 bits.
 * La placa de control presenta un puerto de 16 pines. Las relaciones entre los pines, su nombre en Altium, función en el display y nombre en el código se especifican en la tabla.
 * 
 * 
 * Pin 	| Altium | Variable | Descripción
 * ------------- | ------------- | ------------- | -------------
 * 1	| GND  		| No tiene 	| Masa
 * 2	| VDD		| No tiene 	| Alimentación del display
 * 3  	| VO		| No tiene	| Voltaje variable que representa el brillo, o el contraste, algo de eso.
 * 4	| RS  		| PTAD_PTAD1| Register select. En 1 el dato de 8 bits va al display, en 0, el dato se lee como comando.
 * 5	| GND 		| No tiene 	| Este pin es el R/W, pero se deja en masa porque nunca se leen datos del display.
 * 6	| E 		| PTAD_PTAD0| Enable. En 1 habilita el latch, en 0 lo deshabilita.
 * 7	| Display D0 | PTED 	| Bit del caracter a escribir, o comando a enviar.
 * 8	| Display D1 | PTED 	| Idem
 * 9	| Display D2 | PTED 	| Idem
 * 10	| Display D3 |PTED 		| Idem
 * 11	| Display D4 |PTED 		| Idem
 * 12	| Display D5 |PTED 		| Idem
 * 13	| Display D6 |PTED 		| Idem
 * 14	| Display D7 |PTED 		| Idem
 * 15	| BLA 		| No tiene  | Nivel del backlight
 * 16 	| GND 		| No tiene 	| Masa del backlight
 * 
 * 
 * Instrucciones para manejar el display
 * =======================
 * Estas son las instrucciones que hay que escribir en el puerto de datos, cuando RS está en 0.
 * 
 * D7|D6|D5|D4|D3|D2|D1|D0|Hexa|Significado|Define
 * --|--|--|--|--|--|--|--|----|-----------|------
 * 0 | 0| 0| 0| 0| 0| 0| 0|0x01|Borrar pantalla|LCD_CLEAR_CMD
 * 0 | 0| 0| 0| 0| 1| 1| 0|0x06|Setea la dirección del cursor en modo incremento|LCD_CLEAR_CMD
 * 0 | 0| 0| 0| 1| 0| 0| 0|0x08|No muestra caracteres y no muestra el cursor| No tiene
 * 0 | 0| 0| 0| 1| 0| 1| 0|0x0A|No muestra caracteres y el cursor se muestra pero no parpadea| No tiene
 * 0 | 0| 0| 0| 1| 0| 1| 1|0x0B|No muestra caracteres y el cursor se muestra parpadeando| No tiene
 * 0 | 0| 0| 0| 1| 1| 0| 0|0x0C|Muestra caracteres y no muestra el cursor| No tiene
 * 0 | 0| 0| 0| 1| 1| 1| 0|0x0E|Muestra caracteres y no parpadea el cursor| No tiene 
 * 0 | 0| 0| 0| 1| 1| 1| 1|0x0F|Muestra caracteres y parpadea el cursor| No tiene
 * 0 | 0| 1| 1| 1| 0| 0| 0|0x38|Setea 2 renglones, letra chica y 8 bits de datos|No tiene
 * 0 | 0| 1| 1| 1| 1| 0| 0|0x3C|Setea 2 renglones, letra grande y 8 bits de datos|No tiene
 * 1 | 1| 0| 0| 0| 0| 0| 0|0x80|Selecciona la primera linea para escribir| No tiene
 * 1 | 1| 0| 0| 0| 0| 0| 0|0xC0|Selecciona la segunda linea para escribir|LCD_LF_CMD (el final de linea)
 * 
 */

#include "derivative.h"
#include "LCD.h"
#include "display.h"

extern unsigned char pass,check;
unsigned int contLCDDelay;
unsigned char display_buffer[32];
unsigned char temp[16];
unsigned char minutos; //!< Guarda los minutos de tratamiento, para el reloj que indica el estado.
unsigned char segundos;//!< Guarda los segundos de tratamiento, para el reloj que indica el estado.
 
#define LCD_CLEAR_CMD	0x01 //!< Algo que ver con limpiar el LCD, tal vez el valor que hay que escribir para limpiarlo.
#define LCD_FIRST_LINE_CMD		0x80 //!<Indica que hay que escribir en la primera linea
#define LCD_SECOND_LINE_CMD		0xC0 //!<Indica que hay que escribir en la segunda linea 

/*** Puertos utilizados *****************************/
#define LCD_DATA_PORT	PTED	//!< Puerto de datos
#define LCD_DATA_PORT_D PTEDD	//!< Dirección del puerto (entrada-salida)
#define LCD_CTRL_PORT	PTAD 	//!< Puerto de control


/*** Mascaras ***************************************/
#define LCD_RS_MASK		0x02 //!< Máscara para poner un 1 en el bit 1: 0000 0010
#define LCD_E_MASK		0x01 //!< Máscara para poner un 1 en el bit 0: 0000 0001

/*** Caracteristicas del LCD ***********************/
#define LCD_SIZE    32 //!< Total de caracteres.
#define LINE_LENGTH  16 //!< Largo de cada renglón.


/*
 * demora de un poco mas de 1us.
 * para la frecuencia de 4MHz que está corriendo este soft, entre el llamado a 
 * esta función y el nop, deben ser como 2us.
 */
void demora1us(void){
	asm nop;
}

/*
 * demora de un poco mas de 50us. para asegurar próxima escritura.
 * para la frecuencia de 4MHz que está corriendo este soft, entre el llamado a 
 * esta función y el nop, deben ser como 2us.
 */
void demora50us(void){
	unsigned char i;
	for (i=0;i<20;i++){
		asm nop;
	}
}

/** Envía comando al LCD
 * 
 * \param command El comando que quiero escribir.
 * \param delay1 Un delay entre que escribe el comanod y baja el enable del display.
 * 
 * La secuencia de esta función es poner el bit RS en 0, cargar el comando en el puerto de datos y después habilitar el bit E.
 * Después espera un tiempo dado por delay1 y baja a 0 en enable.
 * */
void lcd_cmd(unsigned char command){
	demora50us();
	LCD_CTRL_PORT &=~(LCD_RS_MASK);
	LCD_DATA_PORT = command;
	LCD_CTRL_PORT |= LCD_E_MASK; 
	demora1us();
	LCD_CTRL_PORT&=~(LCD_E_MASK);
	
}

/** Coloca un caracter en el buffer del display
 * 
 * * Pone en 1 el RS
 * * Escribe el caracter en el puerto de dato
 * * Pone en 1 el E
 * * Espera 1 mseg
 * * Baja el E
 */
void lcd_put (unsigned char dat)
{	
	demora50us();
	LCD_CTRL_PORT|=LCD_RS_MASK;
	LCD_DATA_PORT=dat;  						
	LCD_CTRL_PORT|=LCD_E_MASK;  
	demora1us();
	LCD_CTRL_PORT&=~(LCD_E_MASK);
}
/** Carga un string en el buffer del display.
 * 
 * \param msg Mensaje a escribir, es un array de char
 * \param size Tamaño del mensaje a escribir.
 * 
 * Va recorriendo el msg caracter a caracter, llamando a lcd_put() para cada uno.
 * Detecta cuando llega al final de linea, pasa a segunda linea y sigue.
 * */
void lcd_put_string(unsigned char* msg,unsigned char size){
	unsigned char index=0;
	unsigned char line=0;
	unsigned char i=0;
	
	for(index=0;index<size;index++) {
		lcd_put(*(msg+index));
		if (index>LINE_LENGTH && !line){
			line=1;
			lcd_cmd(0xC0);
			for (i=0;i<100;i++)
				demora50us();
			index--; 
			index--;
		}		
	}
}

/** Limpia el LCD
 * 
 * Envía el comando de limpiar el display.
 * */
void lcd_clear(void){	
	 lcd_cmd(LCD_CLEAR_CMD);
	 contLCDDelay = 3;	// 2ms para volver a mandar un mensaje luego de este comando.
	 while(contLCDDelay);
	 
}

/** Inicializa el LCD
 * 
 * Los comandos que se utilizan están descriptos en la tabla de comandos.
 * */
void LCD_init2(void) {	
	contLCDDelay = 45;
	while(contLCDDelay);
	lcd_cmd(0x38); 
	lcd_cmd(0x38); 
	lcd_cmd(0x38);	
	lcd_cmd(0x06); // Acá setea el modo de escritura.				
	lcd_cmd(LCD_CLEAR_CMD); //Acá lo borra 
	contLCDDelay = 3;
	while(contLCDDelay);	
	lcd_cmd(0x0C); // Lo pone en modo de mostrar caracteres pero no mostrar el cursor.
}

/**  Copia un string constante de FLASH a RAM.
 * 
*/
void str_copy(unsigned char *destino,const unsigned char *origen,unsigned char size) { 
	for( ;size>0;size--) {
		*(destino+size-1)=*(origen+size-1);
	}
}

