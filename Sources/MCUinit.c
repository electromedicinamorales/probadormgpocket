/*
** ###################################################################
**     This code is generated by the Device Initialization Tool.
**     It is overwritten during code generation.
**     USER MODIFICATION ARE PRESERVED ONLY INSIDE INTERRUPT SERVICE ROUTINES
**     OR EXPLICITLY MARKED SECTIONS
**
**     Project     : DeviceInitialization
**     Processor   : MC9S08AC16CFD
**     Version     : Component 01.069, Driver 01.08, CPU db: 3.00.051
**     Datasheet   : MC9S08AC16 Rev. 11 9/2009
**     Date/Time   : 2017-09-20, 14:38, # CodeGen: 1
**     Abstract    :
**         This module contains device initialization code 
**         for selected on-chip peripherals.
**     Contents    :
**         Function "MCU_init" initializes selected peripherals
**
**     Copyright : 1997 - 2014 Freescale Semiconductor, Inc. 
**     All Rights Reserved.
**     
**     Redistribution and use in source and binary forms, with or without modification,
**     are permitted provided that the following conditions are met:
**     
**     o Redistributions of source code must retain the above copyright notice, this list
**       of conditions and the following disclaimer.
**     
**     o Redistributions in binary form must reproduce the above copyright notice, this
**       list of conditions and the following disclaimer in the documentation and/or
**       other materials provided with the distribution.
**     
**     o Neither the name of Freescale Semiconductor, Inc. nor the names of its
**       contributors may be used to endorse or promote products derived from this
**       software without specific prior written permission.
**     
**     THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
**     ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
**     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
**     DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
**     ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
**     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
**     LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
**     ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
**     (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
**     SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**     
**     http: www.freescale.com
**     mail: support@freescale.com
** ###################################################################
*/

/* MODULE MCUinit */

#include <mc9s08ac16.h>                /* I/O map for MC9S08AC16CFD */
#include "MCUinit.h"

/* Standard ANSI C types */
#ifndef int8_t
typedef signed char int8_t;
#endif
#ifndef int16_t
typedef signed int int16_t;
#endif
#ifndef int32_t
typedef signed long int int32_t;
#endif

#ifndef uint8_t
typedef unsigned char uint8_t;
#endif
#ifndef uint16_t
typedef unsigned int uint16_t;
#endif
#ifndef uint32_t
typedef unsigned long int uint32_t;
#endif

/* User declarations and definitions */
#include "mensajes.h"
#include "estadosTest.h"
#include "BUZZER.h"
#include "hardware.h"
extern unsigned int contDelay;
extern unsigned int contLCDDelay;
extern unsigned int contador;
extern estado_t estado;
extern unsigned int medicionLista;
/* End of user declarations and definitions */


/*
** ===================================================================
**     Method      :  Cpu_MCU_init (component MC9S08AC16_48)
*/
/*!
**     @brief
**         Device initialization code for selected peripherals.
*/
/* ===================================================================*/
void MCU_init(void)
{
  /* ### MC9S08AC16_48 "Cpu" init code ... */
  /*  PE initialization code after reset */
  /* Common initialization of the write once registers */
  /* SOPT: COPE=0,COPT=1,STOPE=1 */
  SOPT = 0x73U;                                   
  /* SPMSC1: LVDF=0,LVDACK=0,LVDIE=0,LVDRE=1,LVDSE=1,LVDE=1,BGBE=0 */
  SPMSC1 = 0x1CU;                                   
  /* SPMSC2: LVWF=0,LVWACK=0,LVDV=0,LVWV=0,PPDF=0,PPDACK=0,PPDC=1 */
  SPMSC2 = 0x01U;                                   
  /* SMCLK: MPE=0,MCSEL=0 */
  SMCLK &= (unsigned char)~(unsigned char)0x17U;                     
  /*  System clock initialization */
  /* ICGC1: HGO=0,RANGE=1,REFS=0,CLKS1=0,CLKS0=1,OSCSTEN=1,LOCD=0 */
  ICGC1 = 0x4CU;                                   
  /* ICGC2: LOLRE=0,MFD2=1,MFD1=1,MFD0=1,LOCRE=0,RFD2=0,RFD1=0,RFD0=1 */
  ICGC2 = 0x71U;                                   
  /*lint -save  -e923 Disable MISRA rule (11.3) checking. */
  if (*(unsigned char*far)0xFFBEU != 0xFFU) { /* Test if the device trim value is stored on the specified address */
    ICGTRM = *(unsigned char*far)0xFFBEU; /* Initialize ICGTRM register from a non volatile memory */
  }
  /*lint -restore Enable MISRA rule (11.3) checking. */
  while(ICGS1_LOCK == 0U) {            /* Wait */
  }

  /* Common initialization of the CPU registers */
  /* PTFPE: PTFPE5=0,PTFPE4=0 */
  PTFPE &= (unsigned char)~(unsigned char)0x30U;                     
  /* PTGDD: PTGDD1=0 */
  PTGDD &= (unsigned char)~(unsigned char)0x02U;                     
  /* PTGPE: PTGPE1=1 */
  PTGPE |= (unsigned char)0x02U;                      
  /* PTASE: PTASE7=0,PTASE2=0,PTASE1=0,PTASE0=0 */
  PTASE &= (unsigned char)~(unsigned char)0x87U;                     
  /* PTBSE: PTBSE3=0,PTBSE2=0,PTBSE1=0,PTBSE0=0 */
  PTBSE &= (unsigned char)~(unsigned char)0x0FU;                     
  /* PTCSE: PTCSE5=0,PTCSE4=0,PTCSE3=0,PTCSE2=0,PTCSE1=0,PTCSE0=0 */
  PTCSE &= (unsigned char)~(unsigned char)0x3FU;                     
  /* PTDSE: PTDSE3=0,PTDSE2=0,PTDSE1=0,PTDSE0=0 */
  PTDSE &= (unsigned char)~(unsigned char)0x0FU;                     
  /* PTESE: PTESE7=0,PTESE6=0,PTESE5=0,PTESE4=0,PTESE3=0,PTESE2=0,PTESE1=0,PTESE0=0 */
  PTESE = 0x00U;                                   
  /* PTFSE: PTFSE6=0,PTFSE5=0,PTFSE4=0,PTFSE1=0,PTFSE0=0 */
  PTFSE &= (unsigned char)~(unsigned char)0x73U;                     
  /* PTGSE: PTGSE6=0,PTGSE5=0,PTGSE4=0,PTGSE3=0,PTGSE2=0,PTGSE1=0,PTGSE0=0 */
  PTGSE &= (unsigned char)~(unsigned char)0x7FU;                     
  /* PTADS: PTADS7=0,PTADS2=0,PTADS1=0,PTADS0=0 */
  PTADS = 0x00U;                                   
  /* PTBDS: PTBDS3=0,PTBDS2=0,PTBDS1=0,PTBDS0=0 */
  PTBDS = 0x00U;                                   
  /* PTCDS: PTCDS5=0,PTCDS4=0,PTCDS3=0,PTCDS2=0,PTCDS1=0,PTCDS0=0 */
  PTCDS = 0x00U;                                   
  /* PTDDS: PTDDS3=0,PTDDS2=0,PTDDS1=0,PTDDS0=0 */
  PTDDS = 0x00U;                                   
  /* PTEDS: PTEDS7=0,PTEDS6=0,PTEDS5=0,PTEDS4=0,PTEDS3=0,PTEDS2=0,PTEDS1=0,PTEDS0=0 */
  PTEDS = 0x00U;                                   
  /* PTFDS: PTFDS6=0,PTFDS5=0,PTFDS4=0,PTFDS1=0,PTFDS0=0 */
  PTFDS = 0x00U;                                   
  /* PTGDS: PTGDS6=0,PTGDS5=0,PTGDS4=0,PTGDS3=0,PTGDS2=0,PTGDS1=0,PTGDS0=0 */
  PTGDS = 0x00U;                                   
  /* ### Init_ADC init code */
  /* APCTL1: ADPC3=0,ADPC2=0,ADPC1=0,ADPC0=1 */
  APCTL1 = 0x01U;                                   
  /* ADC1CFG: ADLPC=0,ADIV1=0,ADIV0=0,ADLSMP=0,MODE1=1,MODE0=0,ADICLK1=0,ADICLK0=0 */
  ADC1CFG = 0x08U;                                   
  /* ADC1CV: ADCV9=0,ADCV8=0,ADCV7=0,ADCV6=0,ADCV5=0,ADCV4=0,ADCV3=0,ADCV2=0,ADCV1=0,ADCV0=0 */
  ADC1CV = 0x00U;                            
  /* ADC1SC2: ADACT=0,ADTRG=0,ACFE=0,ACFGT=0 */
  ADC1SC2 = 0x00U;                                   
  /* ADC1SC1: COCO=0,AIEN=0,ADCO=0,ADCH4=1,ADCH3=1,ADCH2=1,ADCH1=1,ADCH0=1 */
  ADC1SC1 = 0x1FU;                                   
  /* ### Init_TPM init code */
  /* TPM1SC: TOF=0,TOIE=0,CPWMS=0,CLKSB=0,CLKSA=0,PS2=0,PS1=0,PS0=0 */
  TPM1SC = 0x00U;                      /* Stop and reset counter */
  TPM1MOD = 0x2710U;                   /* Period value setting */
  (void)(TPM1SC == 0U);                /* Overflow int. flag clearing (first part) */
  /* TPM1SC: TOF=0,TOIE=1,CPWMS=0,CLKSB=0,CLKSA=1,PS2=0,PS1=0,PS0=0 */
  TPM1SC = 0x48U;                      /* Int. flag clearing (2nd part) and timer control register setting */
  /* ### Init_GPIO init code */
  /* PTED: PTED7=0,PTED6=0,PTED5=0,PTED4=0,PTED3=0,PTED2=0,PTED1=0,PTED0=0 */
  PTED = 0x00U;                                   
  /* PTEDD: PTEDD7=1,PTEDD6=1,PTEDD5=1,PTEDD4=1,PTEDD3=1,PTEDD2=1,PTEDD1=1,PTEDD0=1 */
  PTEDD = 0xFFU;                                   
  /* ### Init_GPIO init code */
  /* PTAD: PTAD7=0,PTAD2=0,PTAD1=0,PTAD0=0 */
  PTAD &= (unsigned char)~(unsigned char)0x87U;                     
  /* PTADD: PTADD7=1,PTADD2=1,PTADD1=1,PTADD0=1 */
  PTADD |= (unsigned char)0x87U;                      
  /* ### Init_GPIO init code */
  /* PTFD: PTFD1=0 */
  PTFD &= (unsigned char)~(unsigned char)0x02U;                     
  /* PTFDD: PTFDD1=0 */
  PTFDD &= (unsigned char)~(unsigned char)0x02U;                     
  /* ### Init_GPIO init code */
  /* PTBD: PTBD3=0,PTBD2=0,PTBD1=0 */
  PTBD &= (unsigned char)~(unsigned char)0x0EU;                     
  /* PTBDD: PTBDD3=1,PTBDD2=1,PTBDD1=1 */
  PTBDD |= (unsigned char)0x0EU;                      
  /* ### Init_TPM init code */
  (void)(TPM2C0SC == 0U);              /* Channel 0 int. flag clearing (first part) */
  /* TPM2C0SC: CH0F=0,CH0IE=1,MS0B=0,MS0A=0,ELS0B=0,ELS0A=1 */
  TPM2C0SC = 0x44U;                    /* Int. flag clearing (2nd part) and channel 0 contr. register setting */
  (void)(TPM2C1SC == 0U);              /* Channel 1 int. flag clearing (first part) */
  /* TPM2C1SC: CH1F=0,CH1IE=1,MS1B=0,MS1A=0,ELS1B=0,ELS1A=1 */
  TPM2C1SC = 0x44U;                    /* Int. flag clearing (2nd part) and channel 1 contr. register setting */
  /* TPM2SC: TOF=0,TOIE=0,CPWMS=0,CLKSB=0,CLKSA=0,PS2=0,PS1=0,PS0=0 */
  TPM2SC = 0x00U;                      /* Stop and reset counter */
  TPM2MOD = 0x00U;                     /* Period value setting */
  (void)(TPM2SC == 0U);                /* Overflow int. flag clearing (first part) */
  /* TPM2SC: TOF=0,TOIE=0,CPWMS=0,CLKSB=0,CLKSA=0,PS2=0,PS1=0,PS0=0 */
  TPM2SC = 0x00U;                      /* Int. flag clearing (2nd part) and timer control register setting */
  /* ### Init_KBI init code */
  /* KBISC: KBIE=0 */
  KBISC &= (unsigned char)~(unsigned char)0x02U;                     
  /* KBISC: KBEDG7=0,KBEDG6=0,KBEDG5=0,KBEDG4=0,KBIMOD=0 */
  KBISC &= (unsigned char)~(unsigned char)0xF1U;                     
  /* KBIPE: KBIPE7=0,KBIPE6=0,KBIPE5=0,KBIPE4=0,KBIPE3=0,KBIPE2=0,KBIPE1=1,KBIPE0=0 */
  KBIPE = 0x02U;                                   
  /* KBISC: KBACK=1 */
  KBISC |= (unsigned char)0x04U;                      
  /* KBISC: KBIE=1 */
  KBISC |= (unsigned char)0x02U;                      
  /* ### */
  /*lint -save  -e950 Disable MISRA rule (1.1) checking. */
  asm CLI;                             /* Enable interrupts */
  /*lint -restore Enable MISRA rule (1.1) checking. */

} /*MCU_init*/


/*lint -save  -e765 Disable MISRA rule (8.10) checking. */
/*
** ===================================================================
**     Interrupt handler : isrVadc1
**
**     Description :
**         User interrupt service routine. 
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
__interrupt void isrVadc1(void)
{
  /* Write your interrupt code here ... */

}
/* end of isrVadc1 */


/*
** ===================================================================
**     Interrupt handler : isrVkeyboard1
**
**     Description :
**         User interrupt service routine. 
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
__interrupt void isrVkeyboard1(void)
{
	estado=deteccionEquipo;
	PIN_CARGADOR_ENABLE = 0;
	PTCD_PTCD0 = 0;
	contador=0;
	KBISC_KBACK=1; //Limpio el flag para permitir nuevamente las interrupciones.
}
/* end of isrVkeyboard1 */


/*
** ===================================================================
**     Interrupt handler : isrVtpm2ovf
**
**     Description :
**         User interrupt service routine. 
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
__interrupt void isrVtpm2ovf(void)
{
	//Si entré acá es porque falló la medición de alguno de los PWM, así que indico error.
	estado=falla;

}
/* end of isrVtpm2ovf */


/*
** ===================================================================
**     Interrupt handler : isrVtpm2ch1
**
**     Description :
**         User interrupt service routine. 
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
__interrupt void isrVtpm2ch1(void)
{
	TPM2CNT=0;
	medicionLista++;
	(void) TPM2C1SC;
	TPM2C1SC=0x44;		// limpia flag y configura en rising edge
	if(medicionLista&2){
		TPM2C1SC=0x0;
	}
}
/* end of isrVtpm2ch1 */


/*
** ===================================================================
**     Interrupt handler : isrVtpm2ch0
**
**     Description :
**         User interrupt service routine. 
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
__interrupt void isrVtpm2ch0(void)
{
	TPM2CNT=0;
	medicionLista++;
	(void) TPM2C0SC;
	TPM2C0SC = 0x44;// limpia flag y configura en rising edge
	if(medicionLista&2){
		TPM2C0SC=0x0;
	}
}
/* end of isrVtpm2ch0 */


/*
** ===================================================================
**     Interrupt handler : isrVtpm1ovf
**
**     Description :
**         User interrupt service routine. 
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
__interrupt void isrVtpm1ovf(void)
{
	doBeep();
	if (contDelay)
		contDelay--;
	if (contLCDDelay)
		contLCDDelay--;
	contador++;
	(void) TPM1SC;
	TPM1SC_TOF=0;
}
/* end of isrVtpm1ovf */


/*
** ===================================================================
**     Interrupt handler : isrVicg
**
**     Description :
**         User interrupt service routine. 
**     Parameters  : None
**     Returns     : Nothing
** ===================================================================
*/
__interrupt void isrVicg(void)
{
  /* Write your interrupt code here ... */

}
/* end of isrVicg */


/*lint -restore Enable MISRA rule (8.10) checking. */

/*lint -save  -e950 Disable MISRA rule (1.1) checking. */
/* Initialization of the CPU registers in FLASH */
/* NVPROT: FPS7=1,FPS6=1,FPS5=1,FPS4=1,FPS3=1,FPS2=1,FPS1=1,FPDIS=1 */
static const unsigned char NVPROT_INIT @0x0000FFBDU = 0xFFU;
/* NVOPT: KEYEN=0,FNORED=1,SEC01=1,SEC00=0 */
static const unsigned char NVOPT_INIT @0x0000FFBFU = 0x7EU;
/*lint -restore Enable MISRA rule (1.1) checking. */



extern near void _Startup(void);

/* Interrupt vector table */
#ifndef UNASSIGNED_ISR
  #define UNASSIGNED_ISR ((void(*near const)(void)) 0xFFFF) /* unassigned interrupt service routine */
#endif

/*lint -save  -e923 Disable MISRA rule (11.3) checking. */
/*lint -save  -e950 Disable MISRA rule (1.1) checking. */
static void (* near const _vect[])(void) @0xFFC6 = { /* Interrupt vector table */
/*lint -restore Enable MISRA rule (1.1) checking. */
         UNASSIGNED_ISR,               /* Int.no. 28 Vtpm3ovf (at FFC6)              Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 27 Vtpm3ch1 (at FFC8)              Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 26 Vtpm3ch0 (at FFCA)              Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 25 Vrti (at FFCC)                  Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 24 Viic1 (at FFCE)                 Unassigned */
         isrVadc1,                     /* Int.no. 23 Vadc1 (at FFD0)                 Used */
         isrVkeyboard1,                /* Int.no. 22 Vkeyboard1 (at FFD2)            Used */
         UNASSIGNED_ISR,               /* Int.no. 21 Vsci2tx (at FFD4)               Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 20 Vsci2rx (at FFD6)               Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 19 Vsci2err (at FFD8)              Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 18 Vsci1tx (at FFDA)               Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 17 Vsci1rx (at FFDC)               Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 16 Vsci1err (at FFDE)              Unassigned */
         UNASSIGNED_ISR,               /* Int.no. 15 Vspi1 (at FFE0)                 Unassigned */
         isrVtpm2ovf,                  /* Int.no. 14 Vtpm2ovf (at FFE2)              Used */
         isrVtpm2ch1,                  /* Int.no. 13 Vtpm2ch1 (at FFE4)              Used */
         isrVtpm2ch0,                  /* Int.no. 12 Vtpm2ch0 (at FFE6)              Used */
         isrVtpm1ovf,                  /* Int.no. 11 Vtpm1ovf (at FFE8)              Used */
         UNASSIGNED_ISR,               /* Int.no. 10 VReserved18 (at FFEA)           Unassigned */
         UNASSIGNED_ISR,               /* Int.no.  9 VReserved19 (at FFEC)           Unassigned */
         UNASSIGNED_ISR,               /* Int.no.  8 Vtpm1ch3 (at FFEE)              Unassigned */
         UNASSIGNED_ISR,               /* Int.no.  7 Vtpm1ch2 (at FFF0)              Unassigned */
         UNASSIGNED_ISR,               /* Int.no.  6 Vtpm1ch1 (at FFF2)              Unassigned */
         UNASSIGNED_ISR,               /* Int.no.  5 Vtpm1ch0 (at FFF4)              Unassigned */
         isrVicg,                      /* Int.no.  4 Vicg (at FFF6)                  Used */
         UNASSIGNED_ISR,               /* Int.no.  3 Vlvd (at FFF8)                  Unassigned */
         UNASSIGNED_ISR,               /* Int.no.  2 Virq (at FFFA)                  Unassigned */
         UNASSIGNED_ISR,               /* Int.no.  1 Vswi (at FFFC)                  Unassigned */
         _Startup                      /* Int.no.  0 Vreset (at FFFE)                Reset vector */
};
/*lint -restore Enable MISRA rule (11.3) checking. */




/* END MCUinit */

/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.3 [05.09]
**     for the Freescale HCS08 series of microcontrollers.
**
** ###################################################################
*/
