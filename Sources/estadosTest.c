/*
 * estadosTest.c
 *
 *  Created on: 20-oct-2016
 *      Author: czelayeta
 */
#include "LCD.h"
#include "display.h"
#include "mensajes.h"
#include "BUZZER.h"
#include "estadosTest.h"
#include "hardware.h"

#define CANAL1 0x1
#define CANAL2 0x2



// adc
#define ADQ_VALUE_REG

#define TRUE 1
#define FALSE 0
#define VALOR_ESPERADO 203
#define VALOR_ESPERADO_DC 91

estado_t estado;
unsigned char humano;
unsigned int contador;
unsigned char test_iniciado;
unsigned int contDelay;
extern unsigned int contLCDDelay; 
unsigned int medicionLista;

void testearCorto1(void){
	unsigned int ciclos=0;
	unsigned char estadoCiclo  = 0;
	PIN_CORTO1=1;
	contDelay=200;
	while (contDelay){}
	PIN_CORTO1=0;
	contDelay=50;
	while (contDelay){
		if(!estadoCiclo){
			if(PIN_CH1_PWM==0){
				estadoCiclo = 1;
			}
		}
		else{
			if(PIN_CH1_PWM){
				estadoCiclo = 0;
				ciclos++;
			}
		}
	}
	if(ciclos){
		estado=falla;
		showMessage(vmsgErrorDeteccion[6]);
		return;
	}	
	contDelay=800;	//en este tiempo como máximo debe volver la estimulación.
	if(PIN_CH1_PWM){
		while(PIN_CH1_PWM && contDelay);
	}
	else{
		while(!PIN_CH1_PWM && contDelay);
	}
	if(!contDelay){
		estado=falla;
		showMessage(vmsgErrorDeteccion[9]);
		return;
	}
	contDelay=2;	//en este tiempo como máximo debe volver la estimulación.
	while(contDelay);	// dejo unos ms para ver la señal luego del corto en el osc.	
	estado = testCorto2;
}

void testearCorto2(void){
	unsigned int ciclos=0;
	unsigned char estadoCiclo  = 0;
	//me aseguro de que vuelva a haber estimulación:
	PIN_CORTO2=1;
	contDelay=200;
	while (contDelay){}
	PIN_CORTO2=0;
	contDelay=50;
	while (contDelay){
		if(!estadoCiclo){
			if(PIN_CH2_PWM==0){
				estadoCiclo = 1;
			}
		}
		else{
			if(PIN_CH2_PWM){
				estadoCiclo = 0;
				ciclos++;
			}
		}
	}
	if(ciclos){
		estado=falla;
		showMessage(vmsgErrorDeteccion[7]);
		return;
	}	
	contDelay=200;	//en este tiempo como máximo debe volver la estimulación.
	if(PIN_CH2_PWM){
		while(PIN_CH2_PWM && contDelay);
	}
	else{
		while(!PIN_CH2_PWM && contDelay);
	}
	if(!contDelay){
		estado=falla;
		showMessage(vmsgErrorDeteccion[10]);
		return;
	}
	//volvió a estimular, ahora espero que se apague y voy a siguiente estado.
	contLCDDelay = 2500;
	while(contLCDDelay){
		contDelay = 2;
		while(PIN_CH2_PWM && contDelay);	
		if(contDelay == 0){
			break;		// si durante 2ms no sale de el valor alto -> se detuvo estimulación.
		}
	}
	if(contLCDDelay == 0){	//salió por timeot -> error
		estado=falla;
		showMessage(vmsgErrorDeteccion[16]);
		return;
	}
	//aquí el equipo está sin estimulación y esperando el cargador.
	estado = testAliment;
}

/** 
 * Detecta el comienzo de la estimulación y verifica que sea correcta en ambos canales.
 * Mide el tiempo de los pulsos, 
 */
void medirEstimulacion (void) {
	unsigned int anchoCanal1=0;
	unsigned int anchoCanal2=0;
	unsigned char error=0;
	medicionLista=0;
	TPM1SC_TOIE = 0;	// deshabilito timer 1 si interrumpe cambia la medición.
	TPM2CNT=0;
	TPM2C0SC = 0;
	TPM2C1SC = 0;
	TPM2SC_CLKSA=1;
	TPM2C1SC=0x48;	// pone en cero el tpm2cnt cuando haya falling edge, en la interrupción medición lista cambirá de valor
	while((TPM2CNT<1000) && !(medicionLista & 2));
	TPM2C1SC=0x0;	
	if(medicionLista==0){
		TPM1SC_TOIE = 1;
		estado=falla;
		error = 3;
		showMessage(vmsgErrorDeteccion[error]);
		return;
	}
	anchoCanal1 = TPM2C1V;	
	TPM2SC_CLKSA=0;
	medicionLista = 0;
	TPM2CNT=0;
	TPM2SC_CLKSA=1;
	TPM2C0SC=0x48;	//canal 1 como captura
	while((TPM2CNT<1000) && !(medicionLista & 2));
	TPM2C0SC=0x0;	
	if(!medicionLista){
		TPM1SC_TOIE = 1;
		estado=falla;
		error = 3;
		showMessage(vmsgErrorDeteccion[error]);
		return;
	}
	anchoCanal2=TPM2C0V;
	TPM2SC_CLKSA=0;
	TPM1SC_TOIE = 1;	//vuelve a habilitar timer1
	
	if (anchoCanal1 < (VALOR_ESPERADO-35) || anchoCanal1 > (VALOR_ESPERADO+25)){
		error=error | 0b00000001;
	}
	if (anchoCanal2 < (VALOR_ESPERADO-35) || anchoCanal2 > (VALOR_ESPERADO+25)){
		error=error | 0b00000010;
	}
	if (error > 0){
		estado=falla;
		if((error&0x3) == 3){
			showMessage(vmsgErrorDeteccion[24]);
		}
		else if(error&1){
			showMessage(vmsgErrorDeteccion[22]);			
		}
		else if(error&2){
			showMessage(vmsgErrorDeteccion[23]);
		}
	}
	else {
		estado = testCorto1;
	}
}
	
/**
 * Esta función detecta que existe estimulación en los canales de salida, tiene un timeuot de 15segundos
 * Si alguno de los canales no cambia, o lo hace solo una vez (como si apagan el equipo) reporta el error.
 */
void detectarInicioTest(void){
	unsigned char error=0;
	unsigned char pulsosCH1 = 0;
	unsigned char pulsosCH2 = 0;
	unsigned char estadoPulso;
		
	if (PIN_CH1_PWM == 1){
		error=error | 0b00000001;
	}
	if (PIN_CH2_PWM == 1){
		error=error | 0b00000010;	
	}
	if (contador > 15000 && error>0){
		estado=falla;
		showMessage(vmsgErrorDeteccion[error]);
		return;
	}	
	else if (PIN_CH1_PWM == 0 || PIN_CH2_PWM ==0){	//comienza estimulación ( o se apagó el equipo.)
		error = 0;
		contador=0;
		estadoPulso = 0;
		contDelay = 100;
		while(contDelay);
		contDelay = 1;
		while(contDelay);	
		contDelay = 4;	// en 3 milisegundos, deberían hacerse al menos 120 ( el delay tiene como máximo 4 mili)
		while(contDelay){
			if (PIN_CH1_PWM == 1){
				if(estadoPulso == 0){
					estadoPulso = 1;
					pulsosCH1++;
				}
			}
			else if(PIN_CH1_PWM == 0){
				estadoPulso = 0;
			}
		}
		contDelay = 1;
		while(contDelay); 	//sincronizo que el contDelay = 4 sea de 4 milis igual que el anterior.
		contDelay = 4;	// en 3 milisegundos, deberían hacerse al menos 120 ( el delay tiene como máximo 4 mili)
		while(contDelay){
			if (PIN_CH2_PWM == 1){
				if(estadoPulso == 0){
					estadoPulso = 1;
					pulsosCH2++;
				}
			}
			else if(PIN_CH2_PWM == 0){
				estadoPulso = 0;
			}
		}
		if(pulsosCH1 >120 && pulsosCH2 > 120 ){
			//midió bien ancho de pulso ahora veo que haya estimulación magnética
			// más que nada para detectar por ejemplo si se desconectó la bobina del probador.
			pulsosCH1 = 0;
			pulsosCH2 = 0;
			/*contDelay = 4;	// en 3 milisegundos, deberían hacerse al menos 120 ( el delay tiene como máximo 4 mili)
			while(contDelay){
				if (PIN_ACOPLE1 == 0){
					if(estadoPulso == 0){
						estadoPulso = 1;
						pulsosCH2++;
					}
				}
				else if(PIN_ACOPLE1 == 1){	// el uno es el estado en que menos tiempo permanece
					estadoPulso = 0;
				}
			}	
			contDelay = 4;	// en 3 milisegundos, deberían hacerse al menos 120 ( el delay tiene como máximo 4 mili)
			while(contDelay){
				if (PIN_ACOPLE2 == 0){
					if(estadoPulso == 0){
						estadoPulso = 1;
						pulsosCH2++;
					}
				}
				else if(PIN_ACOPLE2 == 1){	// el uno es el estado en que menos tiempo permanece
					estadoPulso = 0;
				}
			}
			error = 0;
			if(pulsosCH1 < 20){
				error = 4; 
			}
			if(pulsosCH2 < 20){
				error |= 8;
			}
			if(error){
				estado=falla;
				showMessage(vmsgErrorDeteccion[error]);
				return;
			}
			else{*/
				test_iniciado=TRUE;
				showMessage(msgEstimulacionDetectada);
				estado=deteccionEstimMax;
			/*}*/
		}
		else{
			if(pulsosCH1>120 && pulsosCH2 <= 120 ){
				error = 8;	// error en canal 2
			}
			else if (pulsosCH2>120 && pulsosCH1 <= 120){
				error = 4;	// error en canal 1
			}
			else{	// if pulsosch1 pulsos ch2 >120
				error = 12;	// error en canal 1 y 2
			}
			estado=falla;
			showMessage(vmsgErrorDeteccion[error]);
			return;
		}
	}
}

/**
 * Detecta que ambos canales se pongan en alto, que sucede cuando el equipo se enciende
 */
void detectarEquipo(void){
	PIN_CARGADOR_ENABLE = 0;
	if (contador > 15000){
		estado=falla;
		showMessage(vmsgErrorDeteccion[18]);
	}
	else if (PIN_CH1_PWM == 1 || PIN_CH2_PWM ==1){
		contDelay = 8; // 5 milisegundos para evitar errores si un canal baja antes que otro.
		while(contDelay);
		if(PIN_CH1_PWM == 0){
			estado = falla;
			showMessage(vmsgErrorDeteccion[1]);
			return;
		}
		if(PIN_CH2_PWM == 0){
			estado = falla;
			showMessage(vmsgErrorDeteccion[2]);
			return;
		}
		contador=0;
		test_iniciado=TRUE;
		contDelay=1000;
		showMessage(msgEquipoDetectado);
		while (contDelay){}
		estado=deteccionInicioTest;
	}
	
	else if (contador<=1){
		showMessage(msgEsperandoEquipo);
	}
}


/**
 * Conecta el cargador al equipo, este al detectarlo vuelve a estimular la salida para que sea detectado
 * desde el probador.
 */
void conectarFuente(void){
	unsigned int anchoCanal1=0;
	unsigned int anchoCanal2=0;
	unsigned int ciclos;
	unsigned char estadoCiclo;
	unsigned char error=0;
	showMessage(msgAlimentacion);
	contDelay = 50;
	while(contDelay);
	contDelay = 1;
	while(contDelay);	
	PIN_CARGADOR_ENABLE = 1;
	contDelay = 10;	//espero a que se establezca la alimentación 	
	while(contDelay);	
	medicionLista=0;
	TPM1SC_TOIE = 0;	// deshabilito timer 1 si interrumpe cambia la medición.
	TPM2CNT=0;
	TPM2C0SC = 0;
	TPM2C1SC = 0;
	
	TPM2SC_CLKSA=1;
	TPM2C1SC=0x48;	// pone en cero el tpm2cnt cuando haya falling edge, en la interrupción medición lista cambirá de valor
	while((TPM2CNT<65000) && !(medicionLista & 2));
	TPM2C1SC=0x0;	
	if(!medicionLista){
		TPM1SC_TOIE = 1;
		estado=falla;
		error = 3;
		showMessage(vmsgErrorDeteccion[error]);
		return;
	}
	anchoCanal1 = TPM2C1V;	
	TPM2SC_CLKSA=0;
	medicionLista = 0;
	TPM2CNT=0;
	TPM2SC_CLKSA=1;
	TPM2C0SC=0x48;	//canal 1 como captura
	while((TPM2CNT<65000) && !(medicionLista & 2));
	TPM2C0SC=0x0;	
	if(!medicionLista){
		TPM1SC_TOIE = 1;
		estado=falla;
		error = 3;
		showMessage(vmsgErrorDeteccion[error]);
		return;
	}
	anchoCanal2=TPM2C0V;
	TPM2SC_CLKSA=0;
	TPM1SC_TOIE = 1;	//vuelve a habilitar timer1
	
	if (anchoCanal1 < (VALOR_ESPERADO_DC - 15) || anchoCanal1 > (VALOR_ESPERADO_DC +15)){
		error=error | 0b00000001;
	}
	if (anchoCanal2 < (VALOR_ESPERADO_DC - 15) || anchoCanal2 > (VALOR_ESPERADO_DC +15)){
		error=error | 0b00000010;
	}
	if (error > 0){
		estado=falla;
		showMessage(vmsgErrorDeteccion[error]);
	}
	else {
		//espero a que se apague la estimulación
		ciclos = 1;
		while(ciclos){
			contDelay = 1;
			ciclos = 0;
			while(contDelay){	// si durante 1ms no hay ciclos -> terminó estim.
				if(!estadoCiclo){
					if(PIN_CH2_PWM==0){
						estadoCiclo = 1;
					}
				}
				else{
					if(PIN_CH2_PWM){
						estadoCiclo = 0;
						ciclos++;
					}
				}
			}
		}
		estado = waitChargeBatt;
		contador = 0;
	}
}	


/**
 * Este estado espera respuestas desde el pocket, que le dirán si tiene algún problema con la carga de la batería.
 * recibe tres pulsos desde la salida de estimulación del pocket, si son de 1ms es que la prueba está OK
 * si algo anda mal, el pocket envía 4ms.
 * Primero el pocket mide que no exista corriente de carga cuando no debe haber, envía primer pulso
 * Luego pone el mosfet de carga en un 90% de ciclo de actividad, mide la corriente hacia la batería
 * si está dentro de unos valores envía segundo pulso ok
 * Por último mide la tensión de batería dentro de 9 a 12.6v, si está fuera del rango envía error.
 */
void verificarCarga(void){
	contDelay = 5;
	while(contDelay);
	PTCD_PTCD0 = 1;
	TPM1SC_TOIE = 0;	//deshabilito int timer1
	TPM2SC_CLKSA=0;
	medicionLista = 0;
	TPM2CNT=0;
	TPM2SC_CLKSA=1;
	TPM2C0SC=0x48;	//canal 0 como captura falling edge
	while((TPM2CNT<65000) && !(medicionLista & 2));	//pasa 10ms hasta ver algo, si TPM2CNT es 65000 son 16ms
	TPM2C0SC=0x0;	
	if(medicionLista !=2){
		TPM1SC_TOIE = 1;
		estado=falla;
		showMessage(vmsgErrorDeteccion[11]);
		return;
	}
	if (TPM2C0V>12000){
		TPM1SC_TOIE = 1;
		estado=falla;
		showMessage(vmsgErrorDeteccion[13]);
		return;
	}
	TPM2SC_CLKSA=0;
	medicionLista = 0;
	TPM2CNT=0;
	TPM1SC_TOIE = 1;
	contDelay = 5;
	while(contDelay);
	TPM1SC_TOIE = 0;	//deshabilito int timer1
	TPM2SC_CLKSA=1;
	TPM2C0SC=0x48;	//canal 0 como captura	
	while((TPM2CNT<65000) && !(medicionLista & 2));	//pasa 10ms hasta ver algo, si TPM2CNT es 65000 son 16ms
	TPM2C0SC=0x0;	
	if(medicionLista !=2){
		TPM1SC_TOIE = 1;
		estado=falla;
		showMessage(vmsgErrorDeteccion[14]);
		return;
	}
	if (TPM2C0V>12000){
		TPM1SC_TOIE = 1;
		estado=falla;
		showMessage(vmsgErrorDeteccion[14]);
		return;
	}
	TPM2SC_CLKSA=0;
	medicionLista = 0;
	TPM2CNT=0;
	TPM1SC_TOIE = 1;
	contDelay = 5;
	while(contDelay);
	TPM1SC_TOIE = 0;	//deshabilito int timer1
	TPM2SC_CLKSA=1;
	TPM2C0SC=0x48;	//canal 0 como captura	
	while((TPM2CNT<65000) && !(medicionLista & 2));	//pasa 10ms hasta ver algo, si TPM2CNT es 65000 son 16ms
	TPM2C0SC=0x0;	
	if(medicionLista !=2){
		TPM1SC_TOIE = 1;
		estado=falla;
		showMessage(vmsgErrorDeteccion[15]);
		return;
	}
	if (TPM2C0V>12000){
		TPM1SC_TOIE = 1;
		estado=falla;
		showMessage(vmsgErrorDeteccion[15]);
		return;
	}
	TPM1SC_TOIE = 1;
	estado = verEstimMag;
	PIN_CARGADOR_ENABLE = 0;
	
}

/**
 *  Espera a que el equipo se apague
 *  Si el operario no apaga el equipo dentro de los 3 segundos, da error,
 *  La idea de este estado es detectar una falla en la llave de encendido del equipo.
 */
void esperarApagado(void){
	showMessage(msgApagueEquipo);
	contDelay = 3000;	// le da 3 segundos al operario para apagar el equipo.
	while(contDelay){
		if(!PIN_CH1_PWM && !PIN_CH2_PWM){
			estado = testOk;
			return;
		}
	}
	showMessage(vmsgErrorDeteccion[17]);
	estado = falla;
}

/**
 * Detecta el campo magnético generado por las bobinas.
 */
void verCampo(void){
	unsigned char ciclos1=0, ciclos2=0;
	unsigned char estado1=0, estado2= 0;
	
	
	contDelay = 8;	//en 8ms se estabiliza la señal recibidas por las bobinas acopladas, desde ahí se reciben 7 pulsos.
	while(contDelay);
	contador = 0;
	//while(contador<15){
	while(contador<45){
		if(!estado1){
			if(PIN_ACOPLE1){
				estado1 = 1;
			}
		}
		else{
			if(!PIN_ACOPLE1){
				estado1 = 0;
				ciclos1++;
			}
		}
		if(!estado2){
			if(PIN_ACOPLE2){
				estado2 = 1;
			}
		}
		else{
			if(!PIN_ACOPLE2){
				estado2 = 0;
				ciclos2++;
			}
		}		
	}
	if(ciclos2>6 && ciclos1<5){
		showMessage(vmsgErrorDeteccion[19]);
		estado = falla;
	}
	else if(ciclos1>6 && ciclos2<5){
		showMessage(vmsgErrorDeteccion[20]);
		estado = falla;
	}
	else if(ciclos1<5 && ciclos2<5){
		showMessage(vmsgErrorDeteccion[21]);
		estado = falla;		
	}
	else{
		if(ciclos1>12){
			humano = 1;
		}
		else{
			humano = 0;
		}
		estado = waitOff;
	}
	
}

/** Muestra mensaje de equipo ok, además de indicar el tipo de equipo.
 * 
 */
void mensajeEquipoOk(void){
	if(humano){
		showMessage(msgEquipoOkHum);
	}
	else{
		showMessage(msgEquipoOkVet);
	}
	
}
