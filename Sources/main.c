/**
 * @file main.c
 * @brief Rutina principal.
 *  Main del programa
\dot
digraph{

nodesep = 2.5;

inicioMG
inicioProbador

node [shape="box", style=rounded];
	habilitoCargador;
	provocoCorto1;
	provocoCorto2;
	testeoLeds;
	estimulacionMaxima;
	estimulacionMedia;
	estimuloConCargador;
	comienzoCarga;
	enciendoPWM2
	enciendoPWM1_
	enciendoPWM2_
	apagoPWM1
	apagoPWM2
	apagoPWMs


node [shape="diamond", style=rounded];
	detectoInicioMax;        
	detectoEstimMax;
	detectoEstimMedia;
	detectoPWMoff2;
	detectoPWMoff1;
	detectoPWMcarga;
	detectoCorto1;
	detectoCorto2;
	detectoCargador;
	deteccionTeclado;

edge [weight=4]
inicioMG -> deteccionTeclado;
deteccionTeclado -> testeoLeds 
testeoLeds -> estimulacionMaxima 
estimulacionMaxima -> estimulacionMedia 
estimulacionMedia  -> detectoCorto1
detectoCorto1 -> apagoPWM1
apagoPWM1 -> enciendoPWM1_
enciendoPWM1_ -> enciendoPWM2 
enciendoPWM2 -> detectoCorto2
detectoCorto2 -> apagoPWM2
apagoPWM2 -> enciendoPWM2_
enciendoPWM2_ -> apagoPWMs
apagoPWMs->detectoCargador
detectoCargador -> estimuloConCargador
estimuloConCargador -> comienzoCarga

inicioProbador -> detectoInicioMax
detectoInicioMax -> detectoEstimMax
detectoEstimMax -> detectoEstimMedia
detectoEstimMedia -> provocoCorto1
provocoCorto1 -> detectoPWMoff1
detectoPWMoff1 -> detectoPWMon1
detectoPWMon1 -> detectoPWMon2
detectoPWMon2 -> provocoCorto2
provocoCorto2 -> detectoPWMoff2
detectoPWMoff2 -> detectoPWMon2_
detectoPWMon2_ ->habilitoCargador
habilitoCargador -> detectoPWMcarga


{rank = same; inicioMG inicioProbador }
{rank = same; estimulacionMaxima -> detectoInicioMax[style=dashed]}
estimulacionMaxima -> detectoEstimMax[style=dashed]
{rank = same; estimulacionMedia -> detectoEstimMedia [style=dashed]}
{rank = same; detectoCorto1 -> provocoCorto1 [style=dashed, dir="back"]}
{rank = same; apagoPWM1 -> detectoPWMoff1[style=dashed]}
{rank = same; enciendoPWM2 -> detectoPWMon2[style=dashed]}
{rank = same; enciendoPWM2_ -> detectoPWMon2_[style=dashed]}
{rank = same; enciendoPWM1_ -> detectoPWMon1[style=dashed]}
{rank = same; detectoCorto2 -> provocoCorto2 [style=dashed, dir="back"]}
{rank = same; apagoPWM2 -> detectoPWMoff2[style=dashed]}
{rank = same; detectoCargador -> habilitoCargador [style=dashed, dir="back"]}
{rank = same; estimuloConCargador -> detectoPWMcarga[style=dashed]}

}
\enddot
 *  */

#include <hidef.h> /* for EnableInterrupts macro */
#include "derivative.h" /* include peripheral declarations */
#include "LCD.h"
#include "display.h"
#include "estadosTest.h"
#include "mensajes.h"
#include "BUZZER.h"
#include "hardware.h"


#ifdef __cplusplu
extern "C"
#endif


void MCU_init(void); /* Device initialization function declaration */





extern unsigned int contador;
extern unsigned int contDelay;
extern estado_t estado;

void verFuente(void){
	unsigned int muestras;
	unsigned char contadorMuestras;
	muestras = 0;
	for(contadorMuestras=0;contadorMuestras<64;contadorMuestras++){
		ADC1SC1=TENSION_CARGADOR_CH;
		while(!ADC1SC1_COCO); //Espero hasta que el micro diga que finalizó la conversión (va a escribir un 1 en COCO).
		muestras+=ADC1R;
	}
	// con divisor 33K 10K los valores aceptables son 3.72v < v < 4.6v
	if( muestras > VC_MAX){
		showMessage(msgVCargadorA);
		TPM1SC_TOIE = 0;
		asm STOP;
		
	}
	else if(muestras < VC_MIN){
		showMessage(msgVCargadorB);
		TPM1SC_TOIE = 0;
		asm STOP;
	}
}

void main( void) {
	MCU_init();	/* call Device Initialization */
	/* include your code here */
	PTCDD_PTCDD0 = 1;
	
	LCD_init2();
//	clearBuffer();
/*	showMessage(msgMarca);
	contDelay=200;
	while (contDelay){}
	showMessage(msgModelo);
	contDelay=200;
	while (contDelay){}
	showMessage(msgVersion);
	contDelay=200;*/	//no muestro marca ni nada, porque el probador tiene que estar listo ni bien se conecta.
	contDelay=400;
	while(contDelay);
	contador=0;
	
	estado=wait;
	showMessage(msgEsperandoTecla);
	for(;;) {
	  switch (estado){
	  case deteccionEquipo:
		  verFuente();
		  detectarEquipo();
		  break;
	  case deteccionInicioTest:
		  detectarInicioTest();
		  break;
	  case deteccionEstimMax:
		  medirEstimulacion();
		  break;
	  case testCorto1:
		  testearCorto1();
		  break;
	  case testCorto2:
		  testearCorto2();
		  break;
	  case testAliment:
		  conectarFuente();
		  break;
	  case waitChargeBatt:
		  verificarCarga();
		  break;
	  case waitOff:
		  esperarApagado();
		  break;
	  case verEstimMag:
		  verCampo();
	  	  break;
	  case testOk:
		  mensajeEquipoOk();
		  		  
		  asm STOP;	// se apaga hasta presión de botón
		  break;
	  case falla:
		  PIN_CARGADOR_ENABLE = 0;
		  PIN_CORTO1 = 0;
		  PIN_CORTO2 = 0;
		  PTCD_PTCD0 = 0;
		  asm STOP;
		  break;
	  case wait:
		  verFuente();
		  break;
	  default:
		  estado=falla;
		  break;
			  
		  
	  
	  }
    /* __RESET_WATCHDOG(); By default COP is disabled with device init. When enabling, also reset the watchdog. */
  } /* loop forever */
  /* please make sure that you never leave main */
}

