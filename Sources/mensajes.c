/*
 * mensajes.c
 *
 *  Created on: 20-oct-2016
 *      Author: czelayeta
 */


const unsigned char msgMarca  []={"ELEC. MORALES                   "}; //!< Muestra el nombre de la empresa
const unsigned char msgModelo []={"PROBADOR MG POCKET              "};//!< 
const unsigned char msgVersion []={"VERSION 0.2"};					  //!<
const unsigned char msgVCargadorA [] = {"TENSION CARGADORINCORRECTA: ALTA"};//!<
const unsigned char msgVCargadorB [] = {"TENSION CARGADORINCORRECTA: BAJA"};//!<
const unsigned char msgEsperandoTecla []={"PRESIONE TECLA  PARA INICIAR"};//!<
const unsigned char msgEsperandoEquipo []={"DETECTANDO      EQUIPO"};//!<
const unsigned char msgEquipoDetectado []={"EQUIPO          DETECTADO"};//!<
const unsigned char msgEstimulacionDetectada [] = {"ESTIMULACION    DETECTADA"};//!<
const unsigned char msgAlimentacion[] = {"ALIMENTANDO     CON CARGADOR"};//!<
const unsigned char msgApagueEquipo[] = {"APAGUE EL EQUIPO"};
const unsigned char msgEquipoOkVet[] = {" MG VET            EQUIPO OK    " };
const unsigned char msgEquipoOkHum[] = {" MG HUMANO         EQUIPO OK    " };
const unsigned char * const vmsgErrorDeteccion[] = {
  //"                                ",    
	"E000  NO        DEFINIDO",  //0
	"E001 12V CH 1   NO DETECTADO",	//1
	"E002 12V CH 2   NO DETECTADO", //2
	"E003 NO HAY 12V EN LOS CANALES",	//3
	"E004 ESTIM. CH1 NO DETECTADA",	//4
	"E005 EQUIPO OFF ANTES DE TIEMPO", //5
	"E006 CORTO EN   CH1 NO DETECTADO",	//6
	"E007 CORTO EN   CH2 NO DETECTADO", //7
	"E008 ESTIM. CH2 NO DETECTADA    ", //8
	"E009 NO VUELVE AESTIMULAR C1 COR", //9		//LA ESTIMULACIÓN NO VUELVE LUEGO DE ABRIR EL CORTO
	"E010 NO VUELVE AESTIMULAR C2 COR", //10    //LA ESTIMULACIÓN NO VUELVE LUEGO DE ABRIR EL CORTO
	"E011 SIN RESPUES CARGADOR", //11	// EL EQUIPO NO RESPONDE LAS SEÑALES DE MEDICION DE CORRIENTE/TENSION
	"E012 ERROR      SALIDAS", //12
	"E013 CORRIENTE  NO DESEADA", //13		EL EQUIPO TIENEN CORRIENTE HACIA LA BATERÍA CUANDO NO DEBERÍA. 
	"E014 NO CARGA", //14 	NO HAY CORRIENTE HACIA LA BATERÍA
	"E015 TENSION DE BATERIA", //15
	"E016 NO DEJA DE ESTIMULAR", //16
	"E017 EL EQUIPO  NO APAGA", //17
	"E018 EQUIPO NO  DETECTADO", //18
	"E019 CAMPO EN   CANAL1", //19		NO SE DETECTA NADA EN BOBINA DE ACOPLE1 PERO SI EN ACOPLE2
	"E020 CAMPO EN   CANAL2", //20		NO SE DETECTA NADA EN BOBINA DE ACOPLE2 PERO SI EN ACOPLE1
	"E021 CAMPO EN   CANALES", //21		NO SE DETECTA ACOPLE.
	"E022 TIEMPO     CH1",
	"E023 TIEMPO     CH2",
	"E024 TIEMPOS    EN AMBOS CH",
};

//const unsigned char msgEsperandoInicio []={"ESPERANDO INICIO TEST"};//!<

const unsigned char msgMidiendo []={"MIDIENDO        INTENSIDAD"}; //!< 
const unsigned char msgMedicionOK [] =      {"INTENSIDAD MAX  CORRECTA"}; //!<




