/**
 * \file: display.h
 *
 * Intentaré que acá vaya todo lo que tenga que ver con manejar el display.
 * Es probable que este archivo interactúe mucho con keyboarStateMachine.
 */

#ifndef DISPLAY_H_
#define DISPLAY_H_

void clearBuffer(void);
unsigned char getSize(char*);
void scroll(unsigned char);
void scroll_message(unsigned char);
void writeNumber2(char* ,unsigned char ,unsigned char);	
void show_time(unsigned char* ,unsigned char ,unsigned char);
void addString(const unsigned char* ,unsigned char );
void showMessage(const unsigned char* msg);
void showNumber(unsigned char,unsigned char);
void write_dec(unsigned char* ,unsigned char ,unsigned char); 
void copy2Buffer(char* ,char* ,unsigned char );
void writeNumber(unsigned char,unsigned char );
void show (void);
void actualizaHora(unsigned char,unsigned char);

#endif /* DISPLAY_H_ */
