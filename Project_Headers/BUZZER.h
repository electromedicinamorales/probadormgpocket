/**
 * \file BUZZER.h
 *
 *  Created on: 20-oct-2014
 *      Author: pdardano
 */

#ifndef BUZZER_H_
#define BUZZER_H_

#include "derivative.h"

#define BUZZER PTAD_PTAD1 //!< Pin elegido para conectar el buzzer. Si lo pongo en 1 el buzzer suena.

/** Propiedades del buzzer.
 * 
 * Definen una secuencia de pitidos como se explica en la imagen.
 * \image html buzzer.svg
 * En la imagen se ve el esquema del buzzer configurado con *cantidad*=3. Durante timeOn y timeEnd el buzzer está sonando, durante timeOff está apagado. 
 * */
typedef struct {
	
	unsigned int ctOn; //!< Contador decremental que va desde timeOn a cero.
	unsigned int ctOff; //!< Contador decremental que va desde timeOff a cero.
	unsigned int timeOn;//!< Duración del pitido.
	unsigned int timeOff;//!< Duración de la pausa entre pitidos.
	unsigned int tend;//!< Duración el pitido final.
	unsigned char cantidad;//!< Cantidad de cicloss timeOn-timeOff.
	/** Estado del buzzer
	 * 
	 * Puede tener tres estados, no sé bien qué son cada uno.
	 */
	unsigned char state;
}bz;

void setupBuzzer(unsigned char , unsigned int , unsigned int, unsigned int);
unsigned char getBuzzerState(void);
//void buzzerBeep(void);
void doBeep(void);
#endif /* BUZZER_H_ */
