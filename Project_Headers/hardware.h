/**
 * @file hardware.h
 *
 * @date 20-dic-2016
 * @author spaleka
 */

#ifndef HARDWARE_H_
#define HARDWARE_H_

//Entradas
#define PIN_ACOPLE1 PTFD_PTFD1	
#define PIN_ACOPLE2 PTFD_PTFD0
#define PIN_CH1_PWM PTFD_PTFD5
#define PIN_CH2_PWM PTFD_PTFD4
//Salidas
#define PIN_CORTO1 PTBD_PTBD2
#define PIN_CORTO2 PTBD_PTBD3
#define PIN_CARGADOR_ENABLE PTBD_PTBD1 
#define PIN_TENSION_CARGADOR PTBD_PTBD0
#define TENSION_CARGADOR_CH 0

#define VMAX_CARGADOR 20
#define VMIN_CARGADOR 16

// 3040 es la cuenta para:
// divisor resistivo: 1/4.3
// regla de 3 para obtener cuentas equivalentes *1024/5
// tomo 64 muestras *64
// 1/4.3*1024/5*64 = 3048

#define VC_MAX VMAX_CARGADOR*3048
#define VC_MIN VMIN_CARGADOR*3048


#endif /* HARDWARE_H_ */
