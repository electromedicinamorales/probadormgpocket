/** Ver la documentación en LCD.c
 * \file LCD.h
 * \author: pdardano
 */

#ifndef LCD_H_
#define LCD_H_



void LCD_init2(void);

void lcd_put (unsigned char);
void lcd_put_string(unsigned char*,unsigned char);
void lcd_cmd(unsigned char);
void lcd_clear(void);
void str_copy(unsigned char *,const unsigned char *,unsigned char);



#endif /* LCD_H_ */
