/*
 * mensajes.h
 *
 *  Created on: 10-dic-2015
 *      Author: czelayeta
 */

#ifndef MENSAJES_H_
#define MENSAJES_H_

const unsigned char msgMarca  [];
const unsigned char msgModelo [];//!< 
const unsigned char msgVersion [];//!< 
const unsigned char msgVCargadorA []; //!<
const unsigned char msgVCargadorB []; //!<
const unsigned char msgEsperandoTecla [];//!<
const unsigned char msgEsperandoEquipo [];//!<
const unsigned char msgEquipoDetectado [];//!<
const unsigned char msgEstimulacionDetectada [];//!<
const unsigned char msgAlimentacion[];//!<
const unsigned char msgApagueEquipo[];
const unsigned char msgEquipoOkVet[];
const unsigned char msgEquipoOkHum[];
const unsigned char * const vmsgErrorDeteccion[];

//const unsigned char msgEsperandoInicio []={"ESPERANDO INICIO TEST"};//!<

const unsigned char msgMidiendo []; //!< 
const unsigned char msgMedicionOK []; //!<
#endif /* MENSAJES_H_ */
